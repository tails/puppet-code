class monitoring::master (
  String $domainname,
  String $ip,
  String $dbpass,
  String $webdbpass,
  String $apipasswd,
  String $spamaddress,
  String $ticketsalt   = lookup(monitoring::ticketsalt),
) {

# silence debsecan

  # `debsecan` is recommended by `nagios-plugins-contrib` and, unless
  # configured otherwise, will send a noisy daily e-mail report when run by
  # cron.
  augeas { 'debsecan-no-reports':
    context => '/files/etc/default/debsecan',
    changes => 'set REPORT false'
  }

# set up the database

  class { '::mysql::server':
    override_options => {
      'mysqld' => {
        'sql_mode' => 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION,STRICT_ALL_TABLES',
        'ssl_ca'   => undef,
        'ssl_cert' => undef,
        'ssl_key'  => undef,
      },
    },
  }

  mysql::db { 'icinga2':
    user     => 'icinga2',
    password => $dbpass,
    host     => 'localhost',
    charset  => 'utf8mb3',    
    collate  => 'utf8mb3_general_ci',
    grant    => ['SELECT','INSERT','UPDATE','DELETE','DROP','CREATE VIEW','CREATE','INDEX','EXECUTE','ALTER'],
  }

# deploy icinga2

  class { '::icinga2':
    manage_repos => false,
    confd        => true,
    constants    => {
      'NodeName'   => $domainname,
      'ZoneName'   => 'master',
      'TicketSalt' => $ticketsalt,
    },
  }

  class { '::icinga2::feature::idomysql':
    user          => 'icinga2',
    password      => $dbpass,
    database      => 'icinga2',
    import_schema => true,
    require       => Mysql::Db['icinga2'],
  }

  include ::icinga2::pki::ca

  class { '::icinga2::feature::api':
    pki             => 'icinga2',
    ca_host         => $domainname,
    ticket_salt     => $ticketsalt,
    accept_commands => true,
    accept_config   => true,
    endpoints       => { $domainname => {}, },
    zones           => { 
      $domainname => {
        'endpoints' => [ $domainname ],
      },
    },
    ssl_protocolmin => 'TLSv1.2',
  }

  icinga2::object::host{ "${domainname}":
    address => $ip,
    vars    => {
      'os'         => 'Linux',
      'mta'        => true,
      'mailq_warn' => 5,
      'mailq_crit' => 20,
    },
    import  => [ 'generic-host' ],
    target  => '/etc/icinga2/conf.d/hosts.conf',
  }

  icinga2::object::zone { 'global-templates':
    global => true,
  }

# set up all our hosts and services

  include monitoring::master::agents
  include monitoring::master::services
  include monitoring::services::default
  include monitoring::services::puppetmaster
  include monitoring::services::repos_db
  include monitoring::services::mailqueue

# add admin user

  icinga2::object::user { 'icingaadmin':
    email                => $spamaddress,
    enable_notifications => true,
    target               => '/etc/icinga2/conf.d/users.conf',
  }

# add a web interface

  class { 'monitoring::master::web':
    domainname  => $domainname,
    dbpass      => $dbpass,
    webdbpass   => $webdbpass,
    apipasswd   => $apipasswd,
    spamaddress => $spamaddress,
  }

# set up notifications

  icinga2::object::notification { 'testnotification':
    target       => '/etc/icinga2/conf.d/test.conf',
    apply        => true,
    apply_target => 'Service',
    assign       => [ 'host.vars.os == Linux' ],
    users        => ['icingaadmin'],
    command      => 'mail-service-notification',
    states       => [ 'Critical', 'Unknown' ],
    interval     => '0',
  }

}
