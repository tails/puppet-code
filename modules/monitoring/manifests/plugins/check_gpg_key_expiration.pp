class monitoring::plugins::check_gpg_key_expiration (
) {

  file { '/usr/lib/nagios/plugins/check_gpg_key_expiration':
    owner => root,
    group => root,
    mode  => '0755',
    source => 'puppet:///modules/monitoring/check_gpg_key_expiration',
  }

  include monitoring::checkcommands::check_gpg_key_expiration

} 
