class monitoring::plugins::tails_image_http (
) {
  ensure_packages(['curl', 'jq', 'libxml2-utils'])

  file { '/usr/lib/nagios/plugins/check_tails_image_http':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/monitoring/check_tails_image_http',
    require => [
      Package['curl'],
      Package['jq'],
      Package['monitoring-plugins-basic'],
    ]
  }

  include monitoring::checkcommands::tails_image_http
} 
