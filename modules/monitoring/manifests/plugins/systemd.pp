class monitoring::plugins::systemd (
) {

  file { '/usr/lib/nagios/plugins/check_systemd':
    owner => root,
    group => root,
    mode  => '0755',
    source => 'puppet:///modules/monitoring/check_systemd',
  }

} 
