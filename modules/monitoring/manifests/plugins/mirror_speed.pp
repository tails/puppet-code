class monitoring::plugins::mirror_speed (
) {
  ensure_packages( ['wget'] )

  file { '/usr/lib/nagios/plugins/check_mirror_speed':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/monitoring/check_mirror_speed',
    require => Package['wget'],
  }

  include monitoring::checkcommands::mirror_speed
} 
