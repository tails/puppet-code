class monitoring::plugins::puppetmaster (
  Stdlib::Absolutepath $check_puppetdb_nodes_checkout = '/var/lib/nagios/check_puppetdb_nodes',
  String $check_puppetdb_nodes_remote                 = 'https://gitlab.tails.boum.org/tails/check_puppetdb_nodes.git',
) {

  $required_pkgs = [
    'libtimedate-perl',
    'libjson-perl',
    'libmonitoring-plugin-perl',
  ]

  ensure_packages($required_pkgs)

  vcsrepo { $check_puppetdb_nodes_checkout:
    ensure   => 'latest',
    provider => 'git',
    source   => $check_puppetdb_nodes_remote,
    user     => 'nagios',
  }

  # XXX workaround for sysadmin#17988, remove once node is upgraded to bookworm
  tails::profile::git::safe { $check_puppetdb_nodes_checkout: }

  file { '/usr/lib/nagios/plugins/check_puppetmaster':
    ensure  => 'link',
    target  => "${check_puppetdb_nodes_checkout}/check_puppetdb_nodes",
    owner   => root,
    group   => root,
    mode    => '0755',
    notify  => Service['icinga2'],
    require => [
      Package[$required_pkgs],
      Vcsrepo[$check_puppetdb_nodes_checkout],
    ],
  }

  include monitoring::checkcommands::puppetmaster

} 
