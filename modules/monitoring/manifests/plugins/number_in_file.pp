class monitoring::plugins::number_in_file (
) {

  file { '/usr/lib/nagios/plugins/check_number_in_file':
    owner => root,
    group => root,
    mode  => '0755',
    source => 'puppet:///modules/monitoring/check_number_in_file',
  }

  include monitoring::checkcommands::number_in_file

} 
