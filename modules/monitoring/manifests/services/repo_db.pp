define monitoring::services::repo_db (
  String $repo             = $title,
  String $rawname          = $repo,
  String $basedir          = "/srv/apt-snapshots/time-based/repositories/${repo}",
  Integer $packages_warn   = 2000000000,
  Integer $packages_crit   = 3000000000,
  Integer $references_warn = round($packages_warn * 1.5),
  Integer $references_crit = round($packages_crit * 1.5),
) {

  icinga2::object::service{ "${repo}-packages-db":
    check_command    => 'number_in_file',
    vars             => {
      number_in_file_file => "${basedir}/project/packages.db.size",
      number_in_file_warn => $packages_warn,
      number_in_file_crit => $packages_crit,
    }, 
    apply            => true,
    assign           => [ "host.vars.repos.${rawname} == true" ],
    command_endpoint => 'host.name',
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    zone             => lookup(monitoring::master::domainname),
  }
  icinga2::object::service{ "${repo}-references-db":
    check_command    => 'number_in_file',
    vars             => {
      number_in_file_file => "${basedir}/project/references.db.size",
      number_in_file_warn => $references_warn,
      number_in_file_crit => $references_crit,
    }, 
    apply            => true,
    assign           => [ "host.vars.repos.${rawname} == true" ],
    command_endpoint => 'host.name',
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    zone             => lookup(monitoring::master::domainname),
  }

  include monitoring::checkcommands::number_in_file

} 
