class monitoring::services::default (
) {

  $services = lookup('monitoring::default_services',undef,undef,{})

  $services.each |String $service, Variant[Hash,Boolean] $config| {

    $defaults = {
      check_command    => $service,
      apply            => true,
      assign           => [ true ],
      command_endpoint => 'host.name',
      import           => [ 'generic-service' ],
      target           => '/etc/icinga2/conf.d/services.conf',
      zone             => lookup(monitoring::master::domainname),
    }

    if $config {
      if 'srv' in $config {
        $params = deep_merge($defaults,$config['srv'])
      }
      else {
        $params = $defaults
      }
    }
    else {
      $params = $defaults
    }

    create_resources(icinga2::object::service,$service => $params)

  }

  include monitoring::checkcommands::default

}
