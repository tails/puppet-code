class monitoring::services::puppetmaster (
) {

  icinga2::object::service{ 'puppetmaster':
    check_command    => 'puppetmaster',
    apply            => true,
    assign           => [ 'host.vars.puppetmaster == true' ],
    command_endpoint => 'host.name',
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    zone             => lookup(monitoring::master::domainname),
  }

  include monitoring::checkcommands::puppetmaster

} 
