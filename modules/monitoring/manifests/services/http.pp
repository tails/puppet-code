# @summary
#   Setup monitoring of an HTTP endpoint
#
# @param ensure
#   Whether this service should be monitored or not
#
# @param vhost
#   Host name argument for servers using host headers (virtual host)
#
# @param address
#   IP address or name (use numeric address if possible to bypass DNS lookup)
#
# @param https
#   Whether this service is provided via HTTPS or plain HTTP
#
# @param tor
#   Whether this is an onion service or not
#
# @param vars
#   Extra variables to pass to the monitoring object
define monitoring::services::http (
  Enum['absent', 'present']                                $ensure  = 'present',
  Stdlib::Fqdn                                             $vhost   = $name,
  Variant[Stdlib::Fqdn, Stdlib::IP::Address::V4::Nosubnet] $address = $name,
  Boolean                                                  $https   = true,
  Boolean                                                  $tor     = false,
  Hash                                                     $vars    = {},
) {
  $ip_vars = {
    http_ipv4 => true,
    http_ipv6 => false,
  }

  $https_vars = {
    http_port                        => 443,
    http_ssl                         => true,
    http_ssl_force_tlsv1_2_or_higher => true,
  }

  $name_vars = {
    http_vhost   => $vhost,
    http_address => $address,
  }

  $conn_vars = deep_merge($ip_vars, $https ? { false => {}, default => $https_vars })
  $default_vars = deep_merge($conn_vars, $name_vars)
  $final_vars = deep_merge($default_vars, $vars)

  $check_command = $tor ? {
    true    => 'torhttp',
    default => 'http',
  }

  icinga2::object::service{ "http-${name}":
    ensure             => $ensure,
    check_command      => $check_command,
    max_check_attempts => 23,
    host_name          => lookup(monitoring::master::domainname),
    vars               => $final_vars,
    import             => [ 'generic-service' ],
    target             => '/etc/icinga2/conf.d/services.conf',
    zone               => lookup(monitoring::master::domainname),
  }
}
