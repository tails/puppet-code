# @summary
#   Setup monitoring of a TLS endpoint
#
# @param ensure
#   Whether this service should be monitored or not
#
# @param address
#   The host's address
#
# @param cn
#   Pattern to match the CN of the certificate
define monitoring::services::tls (
  Enum['absent', 'present'] $ensure  = 'present',
  String                    $address = $name,
  Array[Stdlib::Fqdn]       $cn      = [$name],
) {
  icinga2::object::service { "tls-${name}":
    ensure         => $ensure,
    check_command  => 'ssl_cert',
    check_interval => 3600,
    host_name      => lookup(monitoring::master::domainname),
    vars           => {
      ssl_cert_address  => $address,
      ssl_cert_cn       => $cn,
      ssl_cert_port     => 443,
      ssl_cert_warn     => 28,
      ssl_cert_critical => 14,
      ssl_cert_sni      => $cn[0],
    },
    import         => ['generic-service'],
    target         => '/etc/icinga2/conf.d/services.conf',
    zone           => lookup(monitoring::master::domainname),
  }
}
