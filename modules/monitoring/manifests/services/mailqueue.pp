class monitoring::services::mailqueue (
) {

  icinga2::object::service{ 'mailqueue':
    check_command    => 'mailqueue',
    apply            => true,
    assign           => [ 'host.vars.mta == true' ],
    command_endpoint => 'host.name',
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    zone             => lookup(monitoring::master::domainname),
  }

  include monitoring::checkcommands::mailqueue

} 
