class monitoring::services::repos_db (
  Hash $agents = lookup(monitoring::agents),
) {

  $agents.each | String $agent, Hash $config | {
 
    if 'vars' in $config {

      $vars = $config['vars']
      if 'repos' in $vars {

        $repos = $vars['repos']
        $repos.each | String $rawname, Variant[Hash,Boolean] $repo_config | {

          if $repo_config {

            $repo = regsubst($rawname,'___','-','G')
            monitoring::services::repo_db { "${repo}":
              rawname => $rawname,
            }
          }
        }
      }
    }
  }
} 
