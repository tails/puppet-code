class monitoring::master::agents (
) {

  $agents = lookup('monitoring::agents',undef,undef,{})

  $agents.each |String $agent, Hash $content| {
    icinga2::object::host{ "${agent}":
      address => $content['address'],
      vars    => $content['vars'],
      import  => [ 'generic-host' ],
      target  => '/etc/icinga2/conf.d/hosts.conf',
    }
    icinga2::object::endpoint{ "${agent}":
      host => $agent,
    }
    icinga2::object::zone{ "${agent}":
      endpoints => [ $agent ],
      parent    => lookup(monitoring::master::domainname),
    }
  }

}
