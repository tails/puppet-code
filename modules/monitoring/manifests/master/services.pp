class monitoring::master::services (
  Hash  $http    = {},
  Hash  $smtp    = {},
  Hash  $dns     = {},
  Array $gpg     = [],
  Array $mirrors = lookup('tails::mirrors', Array, 'first', []),
) {

## services running on the monitoring master

# make sure each host is alive

  icinga2::object::service{ 'hostalive':
    check_command => 'hostalive',
    apply         => true,
    assign        => [ true ],
    import        => [ 'generic-service' ],
    target        => '/etc/icinga2/conf.d/services.conf',
    zone          => lookup(monitoring::master::domainname),
  }

# check our http services

  $http.each | String $site, Hash $config | {
    monitoring::services::http { $site:
      https => $config['https'] ? { undef => true, default => $config['https']},
      tor   => $config['tor'] ? { undef => false, default => $config['tor']},
      vars  => $config['vars'],
    }
  }

  icinga2::object::checkcommand{ "torhttp":
    command   => ["torify", "/usr/lib/nagios/plugins/check_http"],
    import    => [ 'http' ],
    target    => '/etc/icinga2/conf.d/checkcommands.conf',
  }

  Monitoring::Services::Http <<| |>>

# check our smtp services

# make sure we can check onion smtp addresses

  icinga2::object::checkcommand{ "torsmtp":
    command   => ["torify", "/usr/lib/nagios/plugins/check_smtp"],
    import    => [ 'smtp' ],
    target    => '/etc/icinga2/conf.d/checkcommands.conf',
  }


  $smtp.each | String $site, Optional[Hash] $config | {

    $default_vars = {
      'smtp_address' => $site,
    }

    if $config {
      if $config['tor'] {
        $tor = true
      }
      else {
        $tor = false
      }
      $vars = deep_merge($default_vars, $config['vars'])
    }
    else {
      $tor = false
      $vars = $default_vars
    }

    if $tor {
      icinga2::object::service{ "smtp-${site}":
        check_command      => 'torsmtp',
        max_check_attempts => 12,
        check_interval     => '5m',
        retry_interval     => '5m',
        host_name          => lookup(monitoring::master::domainname),
        vars               => $vars,
        import             => [ 'generic-service' ],
        target             => '/etc/icinga2/conf.d/services.conf',
        zone               => lookup(monitoring::master::domainname),
      }
    }
    else{
      icinga2::object::service{ "smtp-${site}":
        check_command      => 'smtp',
        max_check_attempts => 3,
        host_name          => lookup(monitoring::master::domainname),
        vars               => $vars,
        import             => [ 'generic-service' ],
        target             => '/etc/icinga2/conf.d/services.conf',
        zone               => lookup(monitoring::master::domainname),
      }
    }
  }

  Icinga2::Object::Service <<| tag == 'smtp' |>>

# check our dns services

  $dns.each | String $site, Optional[Hash] $config | {

    $default_vars = {
      'dns_lookup' => $site,
    }

    if $config {
      $vars = deep_merge($default_vars,$config['vars'])
    }
    else {
      $vars = $default_vars
    }

    icinga2::object::service{ "dns-${site}":
      check_command      => 'dns',
      max_check_attempts => 3,
      host_name          => lookup(monitoring::master::domainname),
      vars               => $vars,
      import             => [ 'generic-service' ],
      target             => '/etc/icinga2/conf.d/services.conf',
      zone               => lookup(monitoring::master::domainname),
    }

  }

# check our mirror speeds

  $mirrors.each | Hash $mirror | {
    $url = $mirror['url_prefix']
    icinga2::object::service{ "mirror-speed-${url}":
      check_command      => 'mirror_speed',
      max_check_attempts => 3,
      check_interval     => '10m',
      retry_interval     => '5m',
      host_name          => lookup(monitoring::master::domainname),
      vars               => {
        mirror_speed_url => $url,
        mirror_speed_warn => 7,
        mirror_speed_crit => 3,
      },
      import             => [ 'generic-service' ],
      target             => '/etc/icinga2/conf.d/services.conf',
      zone               => lookup(monitoring::master::domainname),
    }
  }

# check our PGP keys

  $gpg.each | String $url | {
    icinga2::object::service{ "check-expiration-${url}":
      check_command      => 'check_gpg_key_expiration',
      max_check_attempts => 3,
      host_name          => lookup(monitoring::master::domainname),
      vars               => {
        check_gpg_key_expiration_url  => $url,
        check_gpg_key_expiration_warn => 60,
        check_gpg_key_expiration_crit => 30,
      },
      import             => [ 'generic-service' ],
      target             => '/etc/icinga2/conf.d/services.conf',
      zone               => lookup(monitoring::master::domainname),
    }
  }

# check Tails image download over HTTP

  icinga2::object::service { "tails-image-http":
    check_command      => "tails_image_http",
    max_check_attempts => 3,
    host_name          => lookup(monitoring::master::domainname),
    import             => [ 'generic-service' ],
    target             => '/etc/icinga2/conf.d/services.conf',
    zone               => lookup(monitoring::master::domainname),
  }

# check TLS certs

  Monitoring::Services::Tls <<| |>>
}
