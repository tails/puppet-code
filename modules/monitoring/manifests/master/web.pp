class monitoring::master::web (
  String $domainname,
  String $dbpass,
  String $webdbpass,
  String $apipasswd,
  String $spamaddress,
) {

# install required packages

  ensure_packages([
    'apache2',
    'php-mysql',
    'python3-certbot-apache',
  ])

# configure apache

  service { 'apache2':
    ensure   => true,
    enable   => true,
    provider => 'systemd',
    require  => Package['apache2'],
  }

  file {'/etc/apache2/conf-available/icingaweb2.conf':
    source  => 'puppet:///modules/icingaweb2/examples/apache2/icingaweb2.conf',
    require => Package['apache2'],
  }

  file {'/etc/apache2/conf-enabled/icingaweb2.conf':
    ensure  => link,
    target  => '/etc/apache2/conf-available/icingaweb2.conf',
    require => File['/etc/apache2/conf-available/icingaweb2.conf'],
    notify  => Service['apache2'],
  }

  file {'/etc/apache2/mods-enabled/rewrite.load':
    ensure  => link,
    target  => '/etc/apache2/mods-available/rewrite.load',
    require => Package['apache2'],
    notify  => Service['apache2'],
  }

  file {'/etc/apache2/mods-enabled/socache_shmcb.load':
    ensure  => link,
    target  => '/etc/apache2/mods-available/socache_shmcb.load',
    require => Package['apache2'],
    notify  => Service['apache2'],
  }

  file {'/etc/apache2/mods-enabled/ssl.load':
    ensure  => link,
    target  => '/etc/apache2/mods-available/ssl.load',
    require => Package['apache2'],
    notify  => Service['apache2'],
  }

  file {'/etc/apache2/mods-enabled/ssl.conf':
    ensure  => link,
    target  => '/etc/apache2/mods-available/ssl.conf',
    require => Package['apache2'],
    notify  => Service['apache2'],
  }

  file { "/etc/apache2/sites-available/${domainname}.conf":
    content => template('monitoring/vhost.conf.erb'),
    require => Package['apache2'],
  }
  file { "/etc/apache2/sites-enabled/${domainname}.conf":
    ensure  => link,
    target  => "/etc/apache2/sites-available/${domainname}.conf",
    require => [ Package['python3-certbot-apache'], File["/etc/apache2/sites-available/${domainname}.conf"] ],
    notify  => Service['apache2'],
  }

# get certificates

  class { letsencrypt:
    email => $spamaddress,
  }

  letsencrypt::certonly { "${domainname}":
    domains => [ $domainname ],
    plugin  => 'apache',
  }

# set up the databases

  include ::mysql::server

  mysql::db { 'icingaweb2':
    user     => 'icingaweb2',
    password => $webdbpass,
    host     => 'localhost',
    charset  => 'utf8mb3',
    collate  => 'utf8mb3_general_ci',
    grant    => ['SELECT','INSERT','UPDATE','DELETE','DROP','CREATE VIEW','CREATE','INDEX','EXECUTE','ALTER','REFERENCES'],
  }

# add api user

  icinga2::object::apiuser { 'icingaweb2':
    password    => $apipasswd,
    permissions => [ "*" ],
    target      => '/etc/icinga2/conf.d/apiusers.conf',
  }

# set up icingaweb

  class { '::icingaweb2':
    manage_repos  => false,
    import_schema => true,
    db_type       => 'mysql',
    db_host       => 'localhost',
    db_port       => 3306,
    db_username   => 'icingaweb2',
    db_password   => $webdbpass,
    require       => Mysql::Db['icingaweb2'],
  }

  icingaweb2::config::resource { 'my-sql':
    type        => 'db',
    db_type     => 'mysql',
    host        => 'localhost',
    port        => 3306,
    db_name     => 'icingaweb2',
    db_username => 'icingaweb2',
    db_password => $webdbpass,
  }

  icingaweb2::config::authmethod { 'my-sql':
    backend  => 'db',
    resource => 'my-sql',
    order    => '01',
  }

  class { 'icingaweb2::module::monitoring':
    ido_host          => 'localhost',
    ido_db_name       => 'icinga2',
    ido_db_username   => 'icinga2',
    ido_db_password   => $dbpass,
    commandtransports => {
      icinga2 => {
        transport => 'api',
        username  => 'icingaweb2',
        password  => $apipasswd,
      },
    },
  }

  firewall { '300 allow incoming HTTP/S (monitoring::master::web)':
    table  => 'filter',
    chain  => 'INPUT',
    proto  => 'tcp',
    dport  => [ 80, 443 ],
    action => 'accept',
  }

}
