class monitoring::checkcommands::mirror_speed (
) {

  icinga2::object::checkcommand{ 'mirror_speed':
    command   => [ '/usr/lib/nagios/plugins/check_mirror_speed' ],
    arguments => {
      '--interval' => '$mirror_speed_interval$',
      '--url'      => '$mirror_speed_url$',
      '--warning'  => '$mirror_speed_warn$',
      '--critical' => '$mirror_speed_crit$',
    },
    target    => '/etc/icinga2/conf.d/checkcommands.conf',
  }

}
