class monitoring::checkcommands::mailqueue (
) {

  icinga2::object::checkcommand{ 'mailqueue':
    command   => [ '/usr/lib/nagios/plugins/check_mailq' ],
    arguments => {
      '-w' => '$mailq_warn$',
      '-c' => '$mailq_crit$',
    },
    target    => '/etc/icinga2/conf.d/checkcommands.conf',
  }

}
