# @summary
#   arrange checkcommands
#
class monitoring::checkcommands::default () {
  $services = lookup('monitoring::default_services',undef,undef,{})

  $services.each |String $service, Variant[Hash,Boolean] $config| {
    if $config {
      if 'cc' in $config {
        icinga2::object::checkcommand{ "${service}":
          command   => $config['cc']['cmd'],
          arguments => $config['cc']['args'],
          vars      => $config['cc']['vars'],
          target    => '/etc/icinga2/conf.d/checkcommands.conf',
        }
      }
    }
  }

  file { '/usr/share/icinga2/include/plugins-contrib.d/systemd.conf':
    ensure => absent,
  }

  include monitoring::plugins::systemd
  include monitoring::plugins::tails_image_http
  include monitoring::plugins::check_gpg_key_expiration
  include monitoring::plugins::mirror_speed
}
