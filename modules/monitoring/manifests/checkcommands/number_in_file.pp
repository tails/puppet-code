class monitoring::checkcommands::number_in_file (
) {

  icinga2::object::checkcommand{ 'number_in_file':
    command   => [ '/usr/lib/nagios/plugins/check_number_in_file' ],
    arguments => {
      '--file'     => '$number_in_file_file$',
      '--warning'  => '$number_in_file_warn$',
      '--critical' => '$number_in_file_crit$',
    },
    target    => '/etc/icinga2/conf.d/checkcommands.conf',
  }

}
