class monitoring::checkcommands::puppetmaster (
) {

  icinga2::object::checkcommand{ 'puppetmaster':
    command   => [ '/usr/lib/nagios/plugins/check_puppetmaster' ],
    arguments => {
      '--apiversion' => '$puppetmaster_api_version$',
      '--warning'    => '$puppetmaster_warn$',
      '--critical'   => '$puppetmaster_crit$',
      '--ignore'     => '$puppetmaster_ignore_nodes$',
    },
    target    => '/etc/icinga2/conf.d/checkcommands.conf',
  }

}
