class monitoring::checkcommands::check_gpg_key_expiration (
) {

  icinga2::object::checkcommand{ 'check_gpg_key_expiration':
    command   => [ '/usr/lib/nagios/plugins/check_gpg_key_expiration' ],
    arguments => {
      '--file'     => '$check_gpg_key_expiration_file$',
      '--url'      => '$check_gpg_key_expiration_url$',
      '--warning'  => '$check_gpg_key_expiration_warn$',
      '--critical' => '$check_gpg_key_expiration_crit$',
    },
    target    => '/etc/icinga2/conf.d/checkcommands.conf',
  }

}
