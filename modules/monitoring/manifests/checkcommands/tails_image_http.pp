class monitoring::checkcommands::tails_image_http (
) {

  icinga2::object::checkcommand{ 'tails_image_http':
    command   => [ '/usr/lib/nagios/plugins/check_tails_image_http' ],
    target    => '/etc/icinga2/conf.d/checkcommands.conf',
  }

}
