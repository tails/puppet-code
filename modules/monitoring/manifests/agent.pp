class monitoring::agent (
  String $domainname = $::fqdn,
  String $master     = lookup(monitoring::master::domainname),
  String $master_ip  = lookup(monitoring::master::ip),
  String $ticketsalt = lookup(monitoring::ticketsalt),
) {

  class { '::icinga2':
    confd     => true,
    features  => ['mainlog'],
    constants  => {
      'NodeName' => $domainname,
      'ZoneName' => $domainname,
    },
  }

  class { '::icinga2::feature::api':
    accept_config   => true,
    accept_commands => true,
    ca_host         => $master_ip,
    ticket_salt     => $ticketsalt,
    endpoints       => {
      'NodeName'               => {},
      $master       => {
        'host' => $master_ip,
      },
    },
    zones           => {
      'ZoneName' => {
        'endpoints' => ['NodeName'],
        'parent'    => $master,
      },
      $master    => {
        'endpoints' => [$master],
      },
    },
  }
  
  icinga2::object::zone { 'global-templates':
    global => true,
  }

  include monitoring::checkcommands::default

}
