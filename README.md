## Editing eyaml data

Eyaml data is protected with a key that doesn't live in this repository.

Assuming you already have access to the Sysadmins' password-store, you need to
manually put the private and public eyaml keys in the `./keys` directory in
order to be able to edit `*.eyaml` files under `hieradata/`:

```
pass tor/eyaml/puppet.lizard/public_key.pkcs7.pem > keys/public_key.pkcs7.pem
pass tor/eyaml/puppet.lizard/private_key.pkcs7.pem > keys/private_key.pkcs7.pem
```

After that, you should be able to edit `*.eyaml` files using the `eyaml` tool
from the `hiera-eyaml` package:

```
sudo apt-get -f install hiera-eyaml
eyaml edit hieradata/common.eyaml
```

### Eyaml data for masterless nodes

Some nodes (eg. `stone`) run Puppet in a "masterless" setup (see
`tails::profile::puppet::masterless`) and use node-specific keys to protect
their eyaml data. For example:

```
NODE=stone.tails.net
mkdir ./keys/${NODE}
pass tor/eyaml/${NODE}/public_key.pkcs7.pem > keys/${NODE}/public_key.pkcs7.pem
pass tor/eyaml/${NODE}/private_key.pkcs7.pem > keys/${NODE}/private_key.pkcs7.pem
eyaml edit \
	--pkcs7-private-key ./keys/${NODE}/private_key.pkcs7.pem \
	--pkcs7-public-key ./keys/${NODE}/public_key.pkcs7.pem \
	hieradata/node/${NODE}.eyaml
```

## Our Puppet Server only accepts signed commits

The remote of this repository that lives in our Puppet Server only accepts
signed commits from sysadmins.

You can check who are the current sysadmins by peeking into the eyaml Hiera
data:

```
eyaml edit hieradata/common.eyaml
```

Look at users defined under `rbac::roles['sysadmin']` and make sure all those
users have a key id set under `rbac::users[$user]`.

On your side, you need to configure Git to sign commits using your OpenPGP key:

```
git config commit.gpgsign true
git config user.signingkey $YOUR_KEY_ID
```

If all of the above is correctly set, then the remote repository in our Puppet
Server should accept your signed commits.
