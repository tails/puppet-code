node 'lizard.tails.net' {
  include tails::profile::base
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::vpn
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::nonfs
  include tails::profile::firewall
  include tails::profile::network
  include tails::profile::munin
  include tails::profile::libvirt
  include tails::profile::backuplvs
  include tails::profile::dropbear
  include tails::profile::modprobe
}

### Guests: *.lizard -----------------------------------------------------------

node 'apt.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::mounts
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::reprepro
  include tails::profile::nonfs
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::firewall
}

node 'apt-proxy.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::mounts
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::firewall
  include tails::profile::apt_cacher_ng
}

node 'bitcoin.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::mounts
  include tails::profile::monitoragent
  include tails::profile::etckeeper
  include tails::profile::puppet::agent
  include tails::profile::bitcoin
  include tails::profile::sysadmins
  include tails::profile::nonfs
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::firewall
}

node 'bittorrent.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::bittorrent
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::firewall
}

node 'dns.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::dns::primary
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::firewall
}

node 'isobuilder1.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::mounts
  include tails::profile::jenkins::isobuilder_only
  include tails::profile::nonfs
  include tails::profile::firewall
}

node 'isobuilder2.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::mounts
  include tails::profile::jenkins::isobuilder_only
  include tails::profile::nonfs
  include tails::profile::firewall
}

node 'isobuilder3.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::mounts
  include tails::profile::jenkins::isobuilder_only
  include tails::profile::nonfs
  include tails::profile::firewall
}

node 'isobuilder4.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::mounts
  include tails::profile::jenkins::isobuilder_only
  include tails::profile::nonfs
  include tails::profile::firewall
}

node 'jenkins.dragon' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::jenkins::master
  include tails::profile::jenkins::reverse_proxy
  include tails::profile::jenkins::artifacts_store
  include tails::profile::firewall
  include tails::profile::nonfs
}

node 'mail.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::monitoragent
  include tails::profile::etckeeper
  include tails::profile::puppet::agent
  include tails::profile::rspamd
  include tails::profile::schleuder
  include tails::profile::mailalias
  include tails::profile::autoreplies
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::firewall
}

node 'misc.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::check_mirrors
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::rss2email
  include tails::profile::check_gpg_monitoring
  include tails::profile::nonfs
  include tails::profile::release_misc
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::firewall
  include tails::profile::mailalias
  include tails::profile::tailsbot

  include tails::profile::jenkins::support::ssh
  include tails::profile::jenkins::support::sftp
}

node 'puppet-git.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::mounts
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::firewall
  include tails::profile::gitolite
}

node 'rsync.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::mounts
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::mirrorbits
  include tails::profile::sysadmins
  include tails::profile::rsync
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::firewall
}

node 'translate.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::weblate
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::firewall
  include tails::profile::mailalias

  # XXX workaround for sysadmin#17988, remove once node is upgraded to Bookworm
  tails::profile::git::safe { "${weblate::params::weblate_data_dir}/vcs/tails/index": }
  tails::profile::git::safe { "${weblate::params::weblate_repos_dir}/integration": }
  tails::profile::git::safe { "${weblate::params::weblate_repos_dir}/staging": }

  # XXX attempt of workaround for sysadmin#17925, maybe migrate to puppet-weblate
  cron { 'Unlock weblate translations':
    command => '/usr/bin/podman exec -t -i weblate weblate unlock_translation tails',
    user    => 'weblate',
    minute  => 0,
    hour    => 1,
  }
}

node 'whisperback.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::whisperback
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::firewall

  # XXX Remove once enough time has passed after deployment of the new address
  # below (see: sysadmin#18080)
  tails::profile::whisperback::onion { 'tails_whisperback_relay':
    public_key    => base64('decode', lookup('whisperback_old_public_key', String, 'first', undef)),
    secret_key    => Sensitive(base64('decode', lookup('whisperback_old_secret_key', String, 'first', undef))),
    onion_address => lookup('whisperback_old_onion_address', String, 'first', undef),
  }

  tails::profile::whisperback::onion { 'whisperback-relay':
    public_key    => base64('decode', lookup('whisperback_public_key', String, 'first', undef)),
    secret_key    => Sensitive(base64('decode', lookup('whisperback_secret_key', String, 'first', undef))),
    onion_address => lookup('whisperback_onion_address', String, 'first', undef),
  }
}

node 'www.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::firewall
  include tails::profile::nginx
  include tails::profile::nonfs
  include tails::profile::nginx::exportcert
  include tails::profile::redmine
  include tails::profile::redmine::redirector
  include tails::profile::weblate::reverse_proxy
  include tails::profile::weblate::staging_reverse_proxy
  include tails::profile::mirrorbits::reverse_proxy

  include tails::profile::website
  include tails::profile::fundraisingfeedback
  include tails::profile::mailalias

  include tails::profile::reprepro::reverse_proxies

  include tails::profile::http_to_git_annex
}

### Other systems managed by lizard's puppetmaster -----------------------------

node 'ecours.tails.net' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::nonfs
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::network
  include tails::profile::vpn
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::mta
  include tails::profile::firewall
  include tails::profile::backupfs
  include tails::profile::monitormaster
  include tails::profile::dropbear
}

node 'gecko.tails.net' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::nonfs
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::vpn
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::firewall
  include tails::profile::backupfs
  include tails::profile::hedgedoc
  include tails::profile::tailsbot
  include tails::profile::monitoragent
  include tails::profile::dropbear
}

node 'teels.tails.net' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::monitoragent
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::dns::secondary
  include tails::profile::network
  include tails::profile::vpn
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::firewall
  include tails::profile::backupfs
}

node 'puppet.lizard' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::puppet::master
  include tails::profile::puppet::enc
  include tails::profile::etckeeper
  include tails::profile::monitoragent
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::rbac
  include monitoring::plugins::puppetmaster
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::firewall
  include tails::profile::sshkeymaster
}

node 'iguana.tails.net' {
  include tails::profile::base
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::vpn
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::monitoragent
  include tails::profile::munin
  include tails::profile::network
  include tails::profile::firewall
  include tails::profile::nonfs
  include tails::profile::libvirt
  include tails::profile::dropbear
}

node 'gitlab-runner.iguana' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::monitoragent
  include tails::profile::network
  include tails::profile::firewall
  include tails::profile::gitlab_runner
  include tails::profile::apt
  include tails::profile::nonfs
  include tails::profile::grub
  include tails::profile::msmtp
}

node 'gitlab-runner2.dragon' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::monitoragent
  include tails::profile::network
  include tails::profile::firewall
  include tails::profile::gitlab_runner
  include tails::profile::apt
  include tails::profile::nonfs
  include tails::profile::grub
  include tails::profile::msmtp
}

node 'skink.tails.net' {
  include tails::profile::base
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::vpn
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::monitoragent
  include tails::profile::network
  include tails::profile::nonfs
  include tails::profile::firewall
  include tails::profile::libvirt
  include tails::profile::dropbear
}

node 'proxy-dev.skink' {
  include tails::profile::apt
  include tails::profile::base
  include tails::profile::etckeeper
  include tails::profile::firewall
  include tails::profile::grub
  include tails::profile::monitoragent
  include tails::profile::msmtp
  include tails::profile::network
  include tails::profile::nginx
  include tails::profile::nonfs
  include tails::profile::puppet::agent
  include tails::profile::rbac
  include tails::profile::sysadmins
}

node 'dragon.tails.net' {
  include tails::profile::base
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::vpn
  include tails::profile::monitoragent
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::network
  include tails::profile::munin
  include tails::profile::firewall
  include tails::profile::msmtp
  include tails::profile::nonfs
  include tails::profile::libvirt
  include tails::profile::backuplvs
  include tails::profile::dropbear
}

node 'isoworker1.dragon' {
  include tails::profile::base
  include tails::profile::testermta
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::firewall
  include tails::profile::network
  include tails::profile::jenkins::isoworker
}

node 'isoworker2.dragon' {
  include tails::profile::base
  include tails::profile::testermta
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::firewall
  include tails::profile::network
  include tails::profile::jenkins::isoworker
}

node 'isoworker3.dragon' {
  include tails::profile::base
  include tails::profile::testermta
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::firewall
  include tails::profile::network
  include tails::profile::jenkins::isoworker
}

node 'isoworker4.dragon' {
  include tails::profile::base
  include tails::profile::testermta
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::firewall
  include tails::profile::network
  include tails::profile::jenkins::isoworker
}

node 'isoworker5.dragon' {
  include tails::profile::base
  include tails::profile::testermta
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::firewall
  include tails::profile::network
  include tails::profile::jenkins::isoworker
}

node 'isoworker6.iguana' {
  include tails::profile::base
  include tails::profile::testermta
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::firewall
  include tails::profile::network
  include tails::profile::jenkins::isoworker
}

node 'isoworker7.iguana' {
  include tails::profile::base
  include tails::profile::testermta
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::firewall
  include tails::profile::network
  include tails::profile::jenkins::isoworker
}

node 'isoworker8.iguana' {
  include tails::profile::base
  include tails::profile::testermta
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::firewall
  include tails::profile::network
  include tails::profile::jenkins::isoworker
}

node 'isoworkers-mail.iguana' {
  include tails::profile::apt
  include tails::profile::base
  include tails::profile::etckeeper
  include tails::profile::firewall
  include tails::profile::grub
  include tails::profile::network
  include tails::profile::nonfs
  include tails::profile::puppet::agent
  include tails::profile::rbac
  include tails::profile::sysadmins
  include tails::profile::jenkins::mail
}

node 'chameleon.tails.net' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::nonfs
  include tails::profile::puppet::agent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::vpn
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::firewall
  include tails::profile::physical
  include tails::profile::libvirt
  include tails::profile::backuplvs
  include tails::profile::monitoragent
  include tails::profile::dropbear
}

node 'mta.chameleon' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::nonfs
  include tails::profile::puppet::agent
  include tails::profile::monitoragent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::firewall
  include tails::profile::schleuder
  include tails::profile::unbound
  include tails::profile::autoreplies
}

node 'www2.chameleon' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::nonfs
  include tails::profile::puppet::agent
  include tails::profile::monitoragent
  include tails::profile::etckeeper
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::firewall
  include tails::profile::msmtp
  include tails::profile::nginx::exportcert
  include tails::profile::mtasts
  include tails::profile::website::mirror
}

node 'testlab.skink' {
  include tails::profile::apt
  include tails::profile::base
  include tails::profile::etckeeper
  include tails::profile::firewall
  include tails::profile::grub
  include tails::profile::msmtp
  include tails::profile::network
  include tails::profile::nonfs
  include tails::profile::puppet::agent
  include tails::profile::rbac
  include tails::profile::sysadmins
}
