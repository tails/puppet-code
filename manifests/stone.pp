node 'stone.tails.net' {
  include tails::profile::base
  include tails::profile::etckeeper
  include tails::profile::monitoragent
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::nonfs
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::network
  include tails::profile::firewall
  include tails::profile::puppet::masterless
  include tails::profile::backupserver
  include tails::profile::dropbear
  include tails::profile::msmtp
}
